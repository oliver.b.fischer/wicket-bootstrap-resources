package com.profitbricks.wicket.webresource.bootstrap3;

import org.apache.wicket.request.resource.CssResourceReference;

public class Bootstrap3JSReference
    extends CssResourceReference
{
    public Bootstrap3JSReference()
    {
        super(Bootstrap3Reference.class, "js/bootstrap.js");
    }
}
