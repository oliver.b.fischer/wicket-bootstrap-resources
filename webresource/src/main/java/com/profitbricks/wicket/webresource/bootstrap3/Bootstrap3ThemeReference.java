package com.profitbricks.wicket.webresource.bootstrap3;

import org.apache.wicket.request.resource.CssResourceReference;

public class Bootstrap3ThemeReference
    extends CssResourceReference
{
    public Bootstrap3ThemeReference()
    {
        super(Bootstrap3Reference.class, "css/bootstrap-theme.css");
    }
}
