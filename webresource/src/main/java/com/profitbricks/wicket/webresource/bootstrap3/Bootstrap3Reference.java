package com.profitbricks.wicket.webresource.bootstrap3;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.CssReferenceHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.PackageResourceReference;

import java.util.Arrays;

import static org.apache.wicket.markup.head.CssHeaderItem.forReference;

public class Bootstrap3Reference
    extends CssResourceReference
{
    public Bootstrap3Reference()
    {
        super(Bootstrap3Reference.class, "css/bootstrap.css");
    }

    @Override
    public Iterable<? extends HeaderItem> getDependencies()
    {
        CssReferenceHeaderItem themeItem =
            CssHeaderItem.forReference(new Bootstrap3ThemeReference());

        JavaScriptReferenceHeaderItem jsItem =
            JavaScriptReferenceHeaderItem.forReference(new Bootstrap3JSReference());

        return Arrays.<HeaderItem>asList(themeItem, jsItem);
    }
}
