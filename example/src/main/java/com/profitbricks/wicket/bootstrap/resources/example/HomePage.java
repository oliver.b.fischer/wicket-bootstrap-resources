package com.profitbricks.wicket.bootstrap.resources.example;

import com.profitbricks.wicket.webresource.bootstrap3.Bootstrap3Reference;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.CssReferenceHeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.internal.HtmlHeaderContainer;
import org.apache.wicket.request.resource.PackageResourceReference;

public final class HomePage extends WebPage
{
    public HomePage() {
    }

    @Override
    public void renderHead(HtmlHeaderContainer container)
    {
        super.renderHead(container);

        JavaScriptReferenceHeaderItem jsRef =
            JavaScriptHeaderItem.forReference(new PackageResourceReference(HomePage.class, "abc.js"));

        CssReferenceHeaderItem cssRef =
            CssHeaderItem.forReference(new Bootstrap3Reference());

        container.getHeaderResponse().render(jsRef);
        container.getHeaderResponse().render(cssRef);
    }
}