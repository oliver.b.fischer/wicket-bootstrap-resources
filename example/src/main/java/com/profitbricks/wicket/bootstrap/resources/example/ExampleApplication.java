package com.profitbricks.wicket.bootstrap.resources.example;

import com.profitbricks.wicket.webresource.bootstrap3.Bootstrap3Reference;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public class ExampleApplication
    extends WebApplication
{
    @Override
    public Class<? extends Page> getHomePage()
    {
        return HomePage.class;
    }

    @Override
    protected void init()
    {
        super.init();

        JavaScriptResourceReference jsrr =
            new JavaScriptResourceReference(HomePage.class, "abc.js");

        getResourceBundles().addJavaScriptBundle(ExampleApplication.class,
                                                 "bundle-abc.js",
                                                 jsrr);

        getResourceBundles().addCssBundle(Bootstrap3Reference.class,
                                          "css/bootstrap.css",
                                          new Bootstrap3Reference());
    }


}
